package com.massivecraft.blockcompression.cmd;

import com.massivecraft.blockcompression.Perm;
import com.massivecraft.blockcompression.Util;
import com.massivecraft.blockcompression.entity.MConf;
import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.collections.MassiveList;
import com.massivecraft.massivecore.command.MassiveCommand;
import com.massivecraft.massivecore.command.requirement.RequirementHasPerm;
import com.massivecraft.massivecore.command.requirement.RequirementIsPlayer;
import com.massivecraft.massivecore.command.type.enumeration.TypeMaterial;
import com.massivecraft.massivecore.mixin.MixinMessage;
import com.massivecraft.massivecore.mson.Mson;
import com.massivecraft.massivecore.util.InventoryUtil;
import com.massivecraft.massivecore.util.Txt;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class CmdCompress extends MassiveCommand
{
    // -------------------------------------------- //
    // INSTANCE
    // -------------------------------------------- //

    private static CmdCompress i = new CmdCompress();
    public static CmdCompress get() { return i; }

    // -------------------------------------------- //
    // CONSTRUCT
    // -------------------------------------------- //

    public CmdCompress()
    {
        // Parameters
        this.addParameter(null, TypeMaterial.get(), "component");

        // Requirements
        this.addRequirements(RequirementIsPlayer.get());
        this.addRequirements(RequirementHasPerm.get(Perm.COMPRESS));
    }

    // -------------------------------------------- //
    // OVERRIDE
    // -------------------------------------------- //

    @Override
    public void perform() throws MassiveException
    {
        Material argMaterial = readArg();

        //Cycle through all of the block types we decompress
        if(argMaterial == null)
        {
            for(Material componentType : MConf.get().getAllComponents())
            {
                compress(sender, componentType);
            }
        }
        else // look for the block we want to compress
        {
            for(Material componentType : MConf.get().getAllComponents())
            {
                // If the arg material is in the config
                if(argMaterial == componentType || argMaterial == MConf.get().getBlock(componentType))
                {
                    compress(sender, componentType);

                    return; // Don't loop through any more of the config since we found what we need
                }
            }
            // Send a message to the user if it's unsuccessful
            MixinMessage.get().messageOne(sender, Mson.parse("<bad>You cannot compress this block."));
        }
    }

    @Override
    public List<String> getTabCompletions(List<String> args, CommandSender sender)
    {
        List<String> ret = new MassiveList<>();

        // Get all of the materials that match
        List<String> allPossible = super.getTabCompletions(args, sender);

        // Now just check for the materials we can actually compress
        for(String i : allPossible)
        {
            for(Material j : MConf.get().getAllComponents())
            {
                if((i.equals(Txt.getNicedEnum(j)) || i.equals(j.toString())) && !ret.contains(i))
                {
                    ret.add(i);
                }
            }
        }

        return ret;
    }

    @Override
    public List<String> getAliases()
    {
        return MConf.get().aliasesCmdCompress;
    }

    // -------------------------------------------- //
    // PRIVATE
    // -------------------------------------------- //

    private static void compress(CommandSender sender, Material componentType)
    {
        // Get the relevant config info
        Material blockType = MConf.get().getBlock(componentType);
        int numComponentsPerBlock = MConf.get().getNumComponents(componentType);

        // Get inventory info, skip if not enough components to compress
        Inventory inventory = ((Player)sender).getInventory();
        int numBlocks = InventoryUtil.countSimilar(inventory, new ItemStack(blockType));
        int numComponents = InventoryUtil.countSimilar(inventory, new ItemStack(componentType));
        if(numComponents < numComponentsPerBlock) return;

        // Remove relevant items from the inventory, we add these back at the end
        inventory.remove(blockType);
        inventory.remove(componentType);

        // Check if we have room to compress the objects
        int numEmptySlots = Util.getNumEmptySlots(inventory);
        boolean isLeftOvers = numComponents % numComponentsPerBlock != 0;
        if(numEmptySlots == 1 && isLeftOvers)
        {
            Util.addItems(inventory, blockType, numBlocks);
            Util.addItems(inventory, componentType, numComponents);
            return;
        }

        // Calculate how many we need to add
        int numBlocksToAdd = numComponents / numComponentsPerBlock;
        int numComponentsToAdd = numComponents % numComponentsPerBlock;

        // And add them
        Util.addItems(inventory, blockType, numBlocksToAdd);
        Util.addItems(inventory, componentType, numComponentsToAdd);
    }
}