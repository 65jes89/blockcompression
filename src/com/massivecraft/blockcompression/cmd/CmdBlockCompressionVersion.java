package com.massivecraft.blockcompression.cmd;

import com.massivecraft.blockcompression.BlockCompression;
import com.massivecraft.blockcompression.Perm;
import com.massivecraft.blockcompression.entity.MConf;
import com.massivecraft.massivecore.command.MassiveCommandVersion;
import com.massivecraft.massivecore.command.requirement.RequirementHasPerm;

import java.util.List;

public class CmdBlockCompressionVersion extends MassiveCommandVersion
{
    // -------------------------------------------- //
    // CONSTRUCT
    // -------------------------------------------- //

    public CmdBlockCompressionVersion()
    {
        super(BlockCompression.get());

        // Requirements
        this.addRequirements(RequirementHasPerm.get(Perm.VERSION));
    }

    // -------------------------------------------- //
    // OVERRIDE
    // -------------------------------------------- //

    @Override
    public List<String> getAliases()
    {
        return MConf.get().aliasesBlockCompressionVersion;
    }
}
