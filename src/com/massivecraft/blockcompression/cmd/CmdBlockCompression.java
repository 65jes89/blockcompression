package com.massivecraft.blockcompression.cmd;

import com.massivecraft.blockcompression.Perm;
import com.massivecraft.blockcompression.entity.MConf;
import com.massivecraft.massivecore.command.MassiveCommand;
import com.massivecraft.massivecore.command.requirement.RequirementHasPerm;

import java.util.List;

public class CmdBlockCompression extends MassiveCommand
{
    // -------------------------------------------- //
    // INSTANCE
    // -------------------------------------------- //

    private static CmdBlockCompression i = new CmdBlockCompression();
    public static CmdBlockCompression get() { return i; }

    // -------------------------------------------- //
    // FIELDS
    // -------------------------------------------- //

    private CmdBlockCompressionConfig cmdBlockCompressionConfig = new CmdBlockCompressionConfig();
    private CmdBlockCompressionVersion cmdBlockCompressionVersion = new CmdBlockCompressionVersion();

    // -------------------------------------------- //
    // CONSTRUCT
    // -------------------------------------------- //

    public CmdBlockCompression()
    {
        // Children
        this.addChild(cmdBlockCompressionConfig);
        this.addChild(cmdBlockCompressionVersion);

        // Requirements
        this.addRequirements(RequirementHasPerm.get(Perm.BASECOMMAND));
    }

    // -------------------------------------------- //
    // OVERRIDE
    // -------------------------------------------- //

    @Override
    public List<String> getAliases()
    {
        return MConf.get().aliasesBlockCompression;
    }
}