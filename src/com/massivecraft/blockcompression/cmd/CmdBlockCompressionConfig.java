package com.massivecraft.blockcompression.cmd;

import com.massivecraft.blockcompression.Perm;
import com.massivecraft.blockcompression.entity.MConf;
import com.massivecraft.massivecore.command.editor.CommandEditSingleton;
import com.massivecraft.massivecore.command.requirement.RequirementHasPerm;

import java.util.List;

public class CmdBlockCompressionConfig extends CommandEditSingleton<MConf>
{
    // -------------------------------------------- //
    // INSTANCE
    // -------------------------------------------- //

    private static CmdBlockCompressionConfig i = new CmdBlockCompressionConfig();
    public static CmdBlockCompressionConfig get() { return i; }

    // -------------------------------------------- //
    // CONSTRUCT
    // -------------------------------------------- //

    public CmdBlockCompressionConfig()
    {
        super(MConf.get()); // Takes care of the config command behavior for us

        // Requirements
        this.addRequirements(RequirementHasPerm.get(Perm.CONFIG));
    }

    // -------------------------------------------- //
    // OVERRIDE
    // -------------------------------------------- //

    @Override
    public List<String> getAliases()
    {
        return MConf.get().aliasesBlockCompressionConfig;
    }
}