package com.massivecraft.blockcompression.cmd;

import com.massivecraft.blockcompression.Perm;
import com.massivecraft.blockcompression.Util;
import com.massivecraft.blockcompression.entity.MConf;
import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.collections.MassiveList;
import com.massivecraft.massivecore.command.MassiveCommand;
import com.massivecraft.massivecore.command.requirement.RequirementHasPerm;
import com.massivecraft.massivecore.command.requirement.RequirementIsPlayer;
import com.massivecraft.massivecore.command.type.enumeration.TypeMaterial;
import com.massivecraft.massivecore.mixin.MixinMessage;
import com.massivecraft.massivecore.mson.Mson;
import com.massivecraft.massivecore.util.InventoryUtil;
import com.massivecraft.massivecore.util.Txt;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class CmdDecompress extends MassiveCommand
{
	// -------------------------------------------- //
	// INSTANCE
	// -------------------------------------------- //

	private static CmdDecompress i = new CmdDecompress();
	public static CmdDecompress get() { return i; }

	// -------------------------------------------- //
	// CONSTRUCT
	// -------------------------------------------- //

	public CmdDecompress()
	{
	    // Parameters
        this.addParameter(null, TypeMaterial.get(), "block");

		// Requirements
		this.addRequirements(RequirementIsPlayer.get());
		this.addRequirements(RequirementHasPerm.get(Perm.DECOMPRESS));
	}

	// -------------------------------------------- //
	// OVERRIDE
	// -------------------------------------------- //

	@Override
	public void perform() throws MassiveException
	{
        Material argMaterial = readArg();

        //Cycle through all of the block types we decompress, if we are decompressing all of them.
        if(argMaterial == null)
        {
            for(Material componentType : MConf.get().getAllComponents())
            {
                decompress(sender, componentType);
            }
        }
        else // look for the block we want to decompress
        {
            for(Material componentType  : MConf.get().getAllComponents())
            {
                // If the arg material is in the config
                if(argMaterial == componentType  || argMaterial == MConf.get().getBlock(componentType))
                {
                    decompress(sender, componentType );

                    return; // Don't loop through any more of the config since we found what we need
                }
            }

            // Send a message to the user if it's unsuccessful
            MixinMessage.get().messageOne(sender, Mson.parse("<bad>You cannot decompress this block."));
        }
	}

    @Override
    public List<String> getTabCompletions(List<String> args, CommandSender sender)
    {
        List<String> ret = new MassiveList<>();

        // Get all of the materials that match
        List<String> allPossible = super.getTabCompletions(args, sender);

        // Now just check for the materials we can actually compress
        for(String i : allPossible)
        {
            for(Material j : MConf.get().getAllBlocks())
            {
                // Check for both the nice CamelCase name, as well as the enum name. Check for duplicates as well.
                if((i.equals(Txt.getNicedEnum(j)) || i.equals(j.toString())) && !ret.contains(i))
                {
                    ret.add(i);
                }
            }
        }

        return ret;
    }

	@Override
	public List<String> getAliases()
	{
		return MConf.get().aliasesCmdDecompress;
	}

    // -------------------------------------------- //
    // PRIVATE
    // -------------------------------------------- //

    // Does the block decompression for a given material
    private static void decompress(CommandSender sender, Material componentType)
    {
        // Get relevant config info
        Material blockType = MConf.get().getBlock(componentType);
        int numComponentsPerBlock = MConf.get().getNumComponents(componentType);

        // Get inventory info, skip if no blocks we care about
        Inventory inventory = ((Player)sender).getInventory();
        int numBlocks = InventoryUtil.countSimilar(inventory, new ItemStack(blockType));
        if(numBlocks == 0) return;
        int numComponents = InventoryUtil.countSimilar(inventory, new ItemStack(componentType));

        // Remove relevant items from the inventory, we add these back at the end
        inventory.remove(blockType);
        inventory.remove(componentType);

        //Decompresses as many full stacks as possible
        while(numBlocks >= blockType.getMaxStackSize() && canDecompress(inventory, blockType.getMaxStackSize(), numComponentsPerBlock, blockType, numBlocks, componentType, numComponents))
        {
            numBlocks -= blockType.getMaxStackSize();
            numComponents += numComponentsPerBlock * blockType.getMaxStackSize();
        }

        //Decompress partial stacks
        int numToDecompress = 0;
        for(int j = blockType.getMaxStackSize() - 1; j > 0; j--)
        {
            //Find the maximum number we can decompress. This will always be less than a full stack.
            if(numBlocks >= j && canDecompress(inventory, j, numComponentsPerBlock, blockType, numBlocks, componentType, numComponents))
            {
                numToDecompress = j;
                break;
            }
        }
        numBlocks -= numToDecompress;
        numComponents += numComponentsPerBlock * numToDecompress;

        //Add the items back into the player's inventory.
        Util.addItems(inventory, blockType, numBlocks);
        Util.addItems(inventory, componentType, numComponents);
    }

	// Returns the number of stacks it takes to fit an item with a given stacksize
	private static int getStacksToFit(int numItems, int stackSize)
	{
		return (numItems + stackSize - 1) / stackSize;
	}

	// Check if it's possible to decompress the given number of blocks into their corresponding components.
	private static boolean canDecompress(Inventory inventory, int numToDecompress, int numComponentsPerBlock, Material blockMaterial, int numBlocks, Material componentMaterial, int numComponents)
    {
        int stacksOfBlocks = getStacksToFit(numBlocks - numToDecompress, blockMaterial.getMaxStackSize());
        int stacksOfComponents = getStacksToFit(numComponents + (numComponentsPerBlock * numToDecompress), componentMaterial.getMaxStackSize());

        return Util.getNumEmptySlots(inventory) >= stacksOfBlocks + stacksOfComponents;
    }
}