package com.massivecraft.blockcompression.cmd.type;

import com.massivecraft.blockcompression.entity.ItemToCompress;
import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.collections.MassiveList;
import com.massivecraft.massivecore.command.type.Type;
import com.massivecraft.massivecore.command.type.combined.TypeCombined;
import com.massivecraft.massivecore.command.type.enumeration.TypeMaterial;
import com.massivecraft.massivecore.command.type.primitive.TypeInteger;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;

import java.util.List;

public class TypeItemToCompress extends TypeCombined<ItemToCompress>
{
    // -------------------------------------------- //
    // INSTANCE
    // -------------------------------------------- //

    private static TypeItemToCompress i = new TypeItemToCompress();
    public static TypeItemToCompress get() { return i; }

    // -------------------------------------------- //
    // CONSTRUCT
    // -------------------------------------------- //

    public TypeItemToCompress()
    {
        super(ItemToCompress.class, TypeMaterial.get(), TypeMaterial.get(), TypeInteger.get());
    }

    // -------------------------------------------- //
    // OVERRIDE
    // -------------------------------------------- //

    @Override
    public List<Object> split(ItemToCompress itemToCompress)
    {
        List<Object> ret = new MassiveList<>();

        ret.add(itemToCompress.component);
        ret.add(itemToCompress.block);
        ret.add(itemToCompress.numComponentsPerBlock);

        return ret;
    }

    @Override
    public List<Object> readParts(String arg, CommandSender sender) throws MassiveException
    {
        // Create
        List<Object> ret = new MassiveList<>();

        // Fill
        List<String> innerArgs = this.getArgs(arg);

        if (innerArgs.size() < this.getInnerTypes().size()) throw new MassiveException().addMsg("<b>Too few arguments.");

        if (innerArgs.size() > this.getInnerTypes().size()) throw new MassiveException().addMsg("<b>Too many arguments.");

        for (int i = 0; i < innerArgs.size(); i++)
        {
            String innerArg = innerArgs.get(i);
            Type<?> innerType = this.getInnerType(getIndexUser(i));
            Object part = innerType.read(innerArg, sender);
            ret.add(part);
        }

        // Return
        return ret;
    }

    @Override
    public ItemToCompress combine(List<Object> args)
    {
        return new ItemToCompress((Material) args.get(0), (Material) args.get(1), (Integer) args.get(2));
    }
}