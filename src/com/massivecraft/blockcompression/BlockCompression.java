package com.massivecraft.blockcompression;

import com.massivecraft.blockcompression.cmd.CmdBlockCompression;
import com.massivecraft.blockcompression.cmd.CmdCompress;
import com.massivecraft.blockcompression.cmd.CmdDecompress;
import com.massivecraft.blockcompression.cmd.type.TypeItemToCompress;
import com.massivecraft.blockcompression.entity.ItemToCompress;
import com.massivecraft.blockcompression.entity.MConfColl;
import com.massivecraft.massivecore.MassivePlugin;
import com.massivecraft.massivecore.command.type.RegistryType;

public class BlockCompression extends MassivePlugin
{
	// -------------------------------------------- //
	// INSTANCE
	// -------------------------------------------- //

	private static BlockCompression i;
	public static BlockCompression get() { return i; }

	// -------------------------------------------- //
	// CONSTRUCT
	// -------------------------------------------- //

	public BlockCompression() { BlockCompression.i = this; }

	// -------------------------------------------- //
	// OVERRIDE
	// -------------------------------------------- //

	@Override
	public void onEnableInner()
	{
		// Types
        RegistryType.register(ItemToCompress.class, TypeItemToCompress.get());

		this.activate(
			// Entities
			MConfColl.class,

			// Commands
			CmdDecompress.class,
			CmdCompress.class,
			CmdBlockCompression.class
		);
	}
}