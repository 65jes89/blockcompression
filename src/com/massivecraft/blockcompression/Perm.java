package com.massivecraft.blockcompression;

import com.massivecraft.massivecore.Identified;
import com.massivecraft.massivecore.util.PermissionUtil;

public enum Perm implements Identified
{
    // -------------------------------------------- //
    // ENUM
    // -------------------------------------------- //
    BASECOMMAND, // the basecommand perm is only used for config and version
    CONFIG,
    VERSION,
    COMPRESS,
    DECOMPRESS,

    // END OF LIST
    ;

    // -------------------------------------------- //
    // FIELDS
    // -------------------------------------------- //

    private final String id;
    @Override public String getId() { return this.id; }

    // -------------------------------------------- //
    // CONSTRUCT
    // -------------------------------------------- //

    Perm()
    {
        this.id = PermissionUtil.createPermissionId(BlockCompression.get(), this);
    }
}