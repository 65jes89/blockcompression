package com.massivecraft.blockcompression.entity;

import com.massivecraft.massivecore.collections.MassiveList;
import com.massivecraft.massivecore.command.editor.annotation.EditorName;
import com.massivecraft.massivecore.store.Entity;
import com.massivecraft.massivecore.util.MUtil;
import org.bukkit.Material;

import java.util.*;

@EditorName("config")
public class MConf extends Entity<MConf>
{
    // -------------------------------------------- //
    // INSTANCE
    // -------------------------------------------- //

    protected static transient MConf i;
    public static MConf get() { return i; }

    // -------------------------------------------- //
    // FIELDS
    // -------------------------------------------- //

    // Aliases
    public List<String> aliasesBlockCompression = MUtil.list("blockcompression");       // Base command aliases, only used for config and version
    public List<String> aliasesBlockCompressionConfig = MUtil.list("config");           // Config command aliases
    public List<String> aliasesBlockCompressionVersion = MUtil.list("version");         // Version command aliases
    public List<String> aliasesCmdDecompress = MUtil.list("decompress");                // Decompress command aliases, no parent command
    public List<String> aliasesCmdCompress = MUtil.list("compress");                    // Compress command aliases, no parent command

    // item, block, number of item
    public List<ItemToCompress> itemsToCompress = new MassiveList<>(
            new ItemToCompress(Material.getMaterial("DIAMOND"), Material.getMaterial("DIAMOND_BLOCK"), 9),
            new ItemToCompress(Material.getMaterial("IRON_INGOT"), Material.getMaterial("IRON_BLOCK"), 9),
            new ItemToCompress(Material.getMaterial("REDSTONE"), Material.getMaterial("REDSTONE_BLOCK"), 9),
            new ItemToCompress(Material.getMaterial("GOLD_INGOT"), Material.getMaterial("GOLD_BLOCK"), 9),
            new ItemToCompress(Material.getMaterial("EMERALD"), Material.getMaterial("EMERALD_BLOCK"), 9),
            new ItemToCompress(Material.getMaterial("QUARTZ"), Material.getMaterial("QUARTZ_BLOCK"), 4),
            new ItemToCompress(Material.getMaterial("GLOWSTONE_DUST"), Material.getMaterial("GLOWSTONE"), 4)
    );

    // -------------------------------------------- //
    // GETTERS
    // -------------------------------------------- //

    public int getNumItemsToConvert()
    {
        return itemsToCompress.size();
    }

    private Material getComponent(int index)
    {
        return itemsToCompress.get(index).component;
    }

    // Get the relevant component based off the blockType. Returns null if not found.
    public Material getComponent(Material blockType)
    {
        for(int i = 0; i < getNumItemsToConvert(); i++)
        {
            if(getBlock(i) == blockType)
            {
                return getBlock(i);
            }
        }
        return null;
    }

    // Returns a list of all of the components we should compress.
    public List<Material> getAllComponents()
    {
        List<Material> ret = new MassiveList<>();
        for(int i = 0; i < getNumItemsToConvert(); i++)
        {
            ret.add(getComponent(i));
        }
        return ret;
    }

    private Material getBlock(int index)
    {
        return itemsToCompress.get(index).block;
    }

    // Get the relevant block based off the componentType. Returns null if not found.
    public Material getBlock(Material componentType)
    {
        for(int i = 0; i < getNumItemsToConvert(); i++)
        {
            if(getComponent(i) == componentType)
            {
                return getBlock(i);
            }
        }
        return null;
    }

    // Returns a list of all of the blocks we should decompress.
    public List<Material> getAllBlocks()
    {
        List<Material> ret = new MassiveList<>();
        for(int i = 0; i < getNumItemsToConvert(); i++)
        {
            ret.add(getBlock(i));
        }
        return ret;
    }

    // Returns the number of materials we can compress/decompress
    private int getNumComponents(int index)
    {
        return itemsToCompress.get(index).numComponentsPerBlock;
    }

    // Get the relevant number of components per block. Returns -1 if not found.
    public int getNumComponents(Material componentType)
    {
        for(int i = 0; i < getNumItemsToConvert(); i++)
        {
            if(getComponent(i) == componentType)
            {
                return getNumComponents(i);
            }
        }
        return -1;
    }
}