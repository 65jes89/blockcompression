package com.massivecraft.blockcompression.entity;

import org.bukkit.Material;

public class ItemToCompress
{
    // -------------------------------------------- //
    // FIELDS
    // -------------------------------------------- //

    public Material component;
    public Material block;
    public int numComponentsPerBlock;

    // -------------------------------------------- //
    // CONSTRUCT
    // -------------------------------------------- //

    public ItemToCompress(Material component, Material block, int numComponentsPerBlock)
    {
        this.component = component;
        this.block = block;
        this.numComponentsPerBlock = numComponentsPerBlock;
    }
}