package com.massivecraft.blockcompression;

import com.massivecraft.massivecore.util.InventoryUtil;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Util
{
    public static int getNumEmptySlots(Inventory inventory)
    {
        int ret = 0;
        for(ItemStack i : inventory.getStorageContents())
        {
            if(InventoryUtil.isNothing(i))
            {
                ret++;
            }
        }
        return ret;
    }

    public static void addItems(Inventory inventory, Material material, int numToAdd)
    {
        int numStacks = numToAdd / material.getMaxStackSize();
        ItemStack stack = new ItemStack(material, material.getMaxStackSize());
        InventoryUtil.addItemTimes(inventory, stack, numStacks);

        int numRemainder = numToAdd % material.getMaxStackSize();
        ItemStack smallStack = new ItemStack(material, numRemainder);
        InventoryUtil.addItemTimes(inventory, smallStack, 1);
    }
}